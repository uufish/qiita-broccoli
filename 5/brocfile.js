html   = require ( "broccoli-jade" )           ( "tree/html" )

style  = require ( "broccoli-stylus" )         ( "tree/style" )

script = require ( "broccoli-webpack-cached" ) ( "tree/script" , {
        entry: {
            index: "index.coffee"
        },
        output: {
            filename: "[name].js",
            chunkFilename: "[id]-chunk.js"
        },
        module: {
            loaders: [
                {
                    test:    /\.coffee$/,
    				loader:  "coffee"
                }
            ]
        },
        resolve: {
            extensions: [ "", ".js", ".coffee" ]
        }
    }
)

module.exports = new require ( "broccoli-merge-trees" ) ( [ html, style, script ] )
