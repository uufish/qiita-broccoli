filter = {
    jade:   require ( "broccoli-jade" ),
    coffee: require ( "broccoli-coffee" ),
    stylus: require ( "broccoli-stylus" )
}

html   = filter.jade   ( "tree/html"   )

script = filter.coffee ( "tree/script" )

style  = filter.stylus ( "tree/style"  )

Merge  = require ( "broccoli-merge-trees" )

module.exports = new Merge ( [ html, script, style ] )


/*
module.exports = new require      ( "broccoli-merge-trees" ) ( [
    require ( "broccoli-jade" )   ( "tree/html"   ),
    require ( "broccoli-coffee" ) ( "tree/script" ),
    require ( "broccoli-stylus" ) ( "tree/style"  )
] )
*/
