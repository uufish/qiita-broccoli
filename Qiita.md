### Broccoliとは

GulpやGruntとは少し設計の違ったタスクランナーです。
差分を計算することで高速なリビルトが可能になり幸せになれます。

### アーキテクチャ

ファイルを編集して保存、自動でコンパイルし出力、さらにリロードといった開発環境を組み立てていくとき、ツリーが複雑になるほどにサイクルは遅くなっていきます。
Broccoliはコードの抽出と実行の単位がツリーにある為に差分を計算することができ高速に稼働します。

    tree   = "tree" // src

    module.exports = tree // dist

いま「tree」というファイルツリーが存在する場合、任意のディレクトリにはツリーが複製されます。

    tree   = "tree" // src

    filter = require ( "broccoli-coffee" )

    tree  = filter  ( tree )

    module.exports = tree // dist

このようにして代替言語や依存関係の問題をフィルターを通すことで解消していきます。

### サンプルについて

サンプルはモジュールを使い回す為にこのような構成になっていて、それぞれの内容は1,2..の階層で分かれています。

    BroccoliSample
    ├── 01
    │   ├── brocfile.js
    │   └── plate
    │       └── index.html
    ├── 02
    │  
    ├── 03
    │  
    ├── node_modules
    │   ├── broccoli
    │   .....
    │
    └── package.json

### インストール

    npm i -D broccoli
    npm i -g broccoli-cli

CUIでBroccoliを走らせるにはbroccoli-cliがグローバルに必要です。

### 目次

1. ブロッコリーを使ってみる
1. ビルドしてみる
1. フィルターを通してみる
1. ツリーを結合してみる
1. webpackを使ってみる

### 1. ブロッコリーを使ってみる

まずbrocfile.jsというファイルをいれます。

    touch brocfile.js

brocfile.jsには次のように書き込みます。

    tree = "tree"

    module.exports = tree

次にフォルダをつくってindex.htmlをいれます。

    mkdir tree
    touch tree/index.html

index.htmlにはこのように書き込みます。

    <p>broccoli</p>

ツリーを監視します。

    broccoli serve

ログに表示されるポートでプレビューされます。

    http://localhost:4200

今回のツリー

    1
    ├── brocfile.js
    └── tree
        └── index.html


### 2. ビルドしてみる

ビルドするには次のようにします。

    broccoli bulid <dist>

<dist>には出力されるディレクトリ名がはいります。
実行するとツリーが生成されます。
既に存在するディレクトリは指定できません、リビルドするにはプラグインが必要です。

    npm install -g broccoli-timepiece

    broccoli-timepiece <dist>

実行すると監視をはじめ、内容が書きかわるごとにリビルドされます。
またリビルドした際にはtmpというディレクトリが生成されることがあります。

今回のツリー

    2
    ├── brocfile.js
    └── tree
    │   └── index.html
    └── public
    │   └── index.html
    └── tmp


### 3. フィルターを通してみる

AltJSはフィルターを通すことでコンパイルすることができます。
ここではCoffeeScriptをいれてみます。

    npm i -D broccoli-coffee

階層をつくりindex.coffeeをいれます。

    mkdir tree/script
    touch tree/script/index.coffee

index.coffeeにはこのように書き込みます。

    console.log "CoffeeScript"

これはのちにindex.jsに変換されます。（index.coffee -> index.js）
index.jsを読み込む為にindex.htmlを書き換えます。

    <p>broccoli</p>
    <script src = "script/index.JS"></script>

brocfile.jsも書き換えます。

    tree   = "tree"

    filter = require ( "broccoli-coffee" )

    tree   = filter  ( tree )

    module.exports = tree

ツリーを監視します。

    broccoli serve

今回のツリー

    3
    ├── brocfile.js
    └── tree
        ├── script
        │   └── index.coffee
        └── index.html


### 4. ツリーを結合してみる

複数のツリーを結合することができます。

    before
    ├── A
    │   └── html
    │
    └── B
    │   └── style
    │
    └── B
        └── script

これがこのようになります。

    after
    └── C
        ├── html
        ├── script
        └── style

プラグインをインストールします。

    npm i -D broccoli-merge-trees

今回は出力したツリーがこのようになるようにしたいと思います。

    public
    ├── index.html
    ├── index.css
    └── index.js

構成を次のように変えます。

    4
    ├── brocfile.js
    └── tree
        ├── html
        │   └── index.jade
        ├── script
        │   └── index.coffee
        └── style
            └── index.styl

フィルターを通したいので追加のプラグインをインストールします。

    npm i -D broccoli-jade broccoli-stylus

Jadeを読み込むのでindex.htmlをindex.jadeにして書き換えます。

    script( src  = "index.JS" )

    link(   href = "index.CSS", rel  = "stylesheet" )

    p broccoli

index.stylはこのように書き込みます。

    p
        color green

フィルターが増えたのでbroccoli.jsも書き換えます。

    filter = {
        jade:   require ( "broccoli-jade" ),
        stylus: require ( "broccoli-stylus" ),
        coffee: require ( "broccoli-coffee" )
    }

    html   = filter.jade   ( "tree/html"   )

    style  = filter.stylus ( "tree/style"  )

    script = filter.coffee ( "tree/script" )

    Merge  = require ( "broccoli-merge-trees" )

    module.exports = new Merge ( [ html, style, script ] )

今回はプラグインが少ないのでこんな感じでも問題ないです。

    module.exports = new require      ( "broccoli-merge-trees" ) ( [
        require ( "broccoli-jade" )   ( "tree/html"   ),
        require ( "broccoli-stylus" ) ( "tree/style"  ),
        require ( "broccoli-coffee" ) ( "tree/script" )
    ] )


ツリーを監視します。

    broccoli serve

今回のツリー

    4
    ├── brocfile.js
    └── tree
        ├── html
        │   └── index.jade
        ├── script
        │   └── index.coffee
        └── style
            └── index.styl


### 5. webpackを使ってみる

モジュール管理すると依存関係が生まれるので追加のプラグインをインストールします。

    npm i -D broccoli-webpack-cached coffee-loader

今回はcoffeeScriptを使っているのでローダーが必要です。

モジュールを読み込むのでindex.coffeeを書き換えます。

    require "./module"

    console.log "CoffeeScript"

同じ階層にmodule.coffeeを入れ書き込みます。

    console.log "This is module."

brocfile.jsも書き換えます。

    html   = require ( "broccoli-jade" )           ( "tree/html" )

    style  = require ( "broccoli-stylus" )         ( "tree/style" )

    script = require ( "broccoli-webpack-cached" ) ( "tree/script" , {
            entry: {
                index: "index.coffee"
            },
            output: {
                filename: "[name].js",
                chunkFilename: "[id]-chunk.js"
            },
            module: {
                loaders: [
                    {
                        test:    /\.coffee$/,
        				loader:  "coffee"
                    }
                ]
            },
            resolve: {
                extensions: [ "", ".js", ".coffee" ]
            }
        }
    )

    module.exports = new require ( "broccoli-merge-trees" ) ( [ html, style, script ] )

今回のツリー

    5
    ├── brocfile.js
    ├── public
    │   ├── index.css
    │   ├── index.html
    │   └── index.js
    ├── tmp
    └── tree
        ├── html
        │   └── index.jade
        ├── script
        │   ├── index.coffee
        │   └── module.coffee
        └── style
            └── index.styl
